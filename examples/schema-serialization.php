<?php

/**
 * Copyright 2021 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

require \dirname(__DIR__) . '/vendor/autoload.php';

try {
    $schema = \Avro\Serde::parseSchema(<<<SCHEMA
{
    "type": "record",
    "name": "Message",
    "namespace": "com.avro",
    "fields": [
        {
            "name": "field1",
            "type": {
                "type": "record",
                "name": "Bar",
                "fields": [
                    { "name": "a_string", "type": "string" }
                ]
            },
            "default": { "a_string": "foo" }
        },
        {
            "name": "field2",
            "type": ["null", "Bar" ]
        }
    ]
}
SCHEMA
    );
    \var_dump($schema);

    echo 'Non canonical form: ' . \Avro\Serde::dumpSchema($schema) . PHP_EOL;
    echo 'Canonical form: ' . \Avro\Serde::dumpCanonicalSchema($schema);
} catch (\Avro\AvroException $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(-1);
}
