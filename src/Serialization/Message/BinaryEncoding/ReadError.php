<?php

/**
 * Copyright 2021 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro\Serialization\Message\BinaryEncoding;

use Avro\AvroException;

class ReadError extends AvroException
{
    public static function notEnoughBytes(int $expected, int $actual): ReadError
    {
        return new self(\sprintf('Cannot read %d bytes, only %d available.', $expected, $actual));
    }

    public static function unknownUnionIndex(int $index): self
    {
        return new self(\sprintf('There is no union-type with index "%d"', $index));
    }
}
